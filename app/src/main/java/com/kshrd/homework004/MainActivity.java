package com.kshrd.homework004;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {
    Button btnDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnDetail=findViewById(R.id.btndetail);

        if(findViewById(R.id.fragment_container) !=null){
            btnDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fragment_container,new NewFragment())
                            .addToBackStack("null").commit();
                }
            });
        }
        if(findViewById(R.id.fragment_container_land) !=null){
            FragmentManager manager=getSupportFragmentManager();
                    manager.beginTransaction()
                            .replace(R.id.fragment_list,new NewFragment())
                            .addToBackStack("null")
                            .commit();
        }
    }
}