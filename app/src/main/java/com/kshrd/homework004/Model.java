package com.kshrd.homework004;

public class Model {
    private String email;

    public Model(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
