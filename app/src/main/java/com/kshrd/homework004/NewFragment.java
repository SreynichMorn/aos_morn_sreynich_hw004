package com.kshrd.homework004;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class NewFragment extends Fragment {
    RecyclerView recyclerView;
    List<Model>list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.new_fragment,container,false);
        recyclerView=view.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(new ItemAdapter(initData(),getContext()));

        return view;
    }

    private List<Model> initData() {
        list=new ArrayList<>();
        list.add(new Model("sreynichmorn09@gmail.com"));
        list.add(new Model("mina@gmail.com"));
        list.add(new Model("leakena@gmail.com"));
        list.add(new Model("pheaktra001@gmail.com"));
        list.add(new Model("likamana22@gmail.com"));
        list.add(new Model("lyly099@gmail.com"));
        list.add(new Model("molika02@gmail.com"));
        list.add(new Model("makara03@gmail.com"));
        list.add(new Model("likanita32@gmail.com"));
        list.add(new Model("sreynichmorn09@gmail.com"));
        list.add(new Model("mina@gmail.com"));
        list.add(new Model("leakena@gmail.com"));
        list.add(new Model("pheaktra001@gmail.com"));
        list.add(new Model("likamana22@gmail.com"));
        list.add(new Model("lyly099@gmail.com"));
        list.add(new Model("molika02@gmail.com"));
        list.add(new Model("makara03@gmail.com"));
        list.add(new Model("likanita32@gmail.com"));


        return list;
    }
}
